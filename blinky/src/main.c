/*
 * This program is nothing more than a simple test to blink LEDs.
 *
 * Technically it does a few more things. I wanted to test CCM and the heap,
 * so there's code in here for that.
 *
 * I also toggle a few pins high and low because I eventually connected a
 * logic analyzer to it (Saanlima Pipistrello LX45 running an LA program), and
 * needed a way to test it.
 *
 * The pointless variables exist simply to be examined by GDB to verify memory
 * is being initialized properly.
 */


#include <stddef.h>
#include <stdlib.h>
#include <stm32f4xx.h>
#include <stm32f4xx_rcc.h>
#include <stm32f4xx_gpio.h>
#include <FreeRTOS.h>
#include <task.h>


// For toggling pins
typedef struct {
	GPIO_TypeDef *port;
	uint16_t pin;
	TickType_t delay;
} pin_t;

extern char _snoinit;


// Testing global and static memory
int g1;
int g2 = 3;

static const int g3 = 1;


// Each task uses this function to toggle its pin
static void led(void *p) {

	pin_t *pin = p;

	while (1) {
		g1 += g3;
		g2++;

		GPIO_ToggleBits(pin->port, pin->pin);
		vTaskDelay(pin->delay * 10);
	}
}

static void Reset_Handler2(void);


// LED pins
static const pin_t led0 = {
	.port = GPIOD,
	.pin = GPIO_Pin_13,
	.delay = 250
};

static const pin_t led1 = {
	.port = GPIOD,
	.pin = GPIO_Pin_12,
	.delay = 500
};

static const pin_t led2 = {
	.port = GPIOD,
	.pin = GPIO_Pin_14,
	.delay = 750
};

static const pin_t led3 = {
	.port = GPIOD,
	.pin = GPIO_Pin_15,
	.delay = 1000
};


// LA pins
static pin_t la0 __attribute__ ((section(".ccm"))) = {
	.port = GPIOA,
	.pin = GPIO_Pin_1,
	.delay = 1
};

static pin_t la1 __attribute__ ((section(".ccm"))) = {
	.port = GPIOA,
	.pin = GPIO_Pin_5,
	.delay = 2
};

static pin_t la2 __attribute__ ((section(".ccm"))) = {
	.port = GPIOC,
	.pin = GPIO_Pin_5,
	.delay = 3
};


// Testing that CCM variables actually get initialized before main()
static uint32_t vars[500] __attribute__ ((section(".ccm"))) = {
	1, 2, 3, 4, 5
};

static int32_t stuff[500] = { 1 };

static void test_fn(void) {
	stuff[100] = 7;
	Reset_Handler2();
}
/**
 * what happens if
 * i write a bunch
 * of documentation?
 *
 * @brief testing doxygen
 * @return nothing
 * @retval hmmm
 * @note here's a note
 * @attention to somebody
 * @exception some error
 * @param nothing: some arg
 * @param another param
 */
static void Reset_Handler2(void) {
	g1 += 5;
}

int main(void) {

	vars[5] = 6;
	stuff[5] = 6;

	float f = 7.5 * 2.56;

	f *= 3.14159f;

	test_fn();

	Reset_Handler2();

	uint32_t cd = *(uint32_t*)&_snoinit;

	// http://www.freertos.org/RTOS-Cortex-M3-M4.html
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);

	// Enable clocks and configure pins
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA
				| RCC_AHB1Periph_GPIOC
				| RCC_AHB1Periph_GPIOD, ENABLE);

	// *(uint32_t*)&_snoinit = 0xdeadbeef;
	// NVIC_SystemReset();

	GPIO_InitTypeDef gpio;
	TIM_TimeBaseInitTypeDef tp;

	/*

	Bunch of code to test various features of timers

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

	GPIO_PinAFConfig(GPIOA, GPIO_PinSource0, GPIO_AF_TIM2);

	gpio.GPIO_Pin = GPIO_Pin_0;
	gpio.GPIO_OType = GPIO_OType_PP;
	gpio.GPIO_PuPd = GPIO_PuPd_NOPULL;
	gpio.GPIO_Mode = GPIO_Mode_AF;
	gpio.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOA, &gpio);

	tp.TIM_Prescaler = 84 - 1;
	tp.TIM_CounterMode = TIM_CounterMode_Up;
	tp.TIM_Period = 10 - 1;
	tp.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInit(TIM2, &tp);


	TIM_OCInitTypeDef oc5;

	oc5.TIM_OCMode = TIM_OCMode_PWM1;
	oc5.TIM_OutputState = TIM_OutputState_Enable;
	oc5.TIM_OCPolarity = TIM_OCPolarity_High;
	oc5.TIM_Pulse = 1;

	TIM_OC1Init(TIM2, &oc5);
	TIM_Cmd(TIM2, ENABLE);

	while (1);


	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);


	GPIO_PinAFConfig(GPIOA, GPIO_PinSource8, GPIO_AF_TIM1);

	gpio.GPIO_Pin = GPIO_Pin_8;
	gpio.GPIO_OType = GPIO_OType_PP;
	gpio.GPIO_PuPd = GPIO_PuPd_NOPULL;
	gpio.GPIO_Mode = GPIO_Mode_AF;
	gpio.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOA, &gpio);


	TIM_OCInitTypeDef oc3;

	oc3.TIM_OCMode = TIM_OCMode_PWM1;
	oc3.TIM_OutputState = TIM_OutputState_Enable;
	oc3.TIM_OutputNState = TIM_OutputNState_Enable;
	oc3.TIM_OCPolarity = TIM_OCPolarity_Low;
	oc3.TIM_OCNPolarity = TIM_OCNPolarity_Low;
	oc3.TIM_OCIdleState = TIM_OCIdleState_Reset;
	oc3.TIM_OCNIdleState = TIM_OCNIdleState_Reset;
	oc3.TIM_Pulse = 800;

	TIM_OC1Init(TIM1, &oc3);
	TIM_CtrlPWMOutputs(TIM1, ENABLE);

	//TIM_SelectOnePulseMode(TIM1, TIM_OPMode_Repetitive);


	tp.TIM_Prescaler = 0;
	tp.TIM_CounterMode = TIM_CounterMode_Up;
	tp.TIM_Period = 1680;
	tp.TIM_ClockDivision = TIM_CKD_DIV1;
	tp.TIM_RepetitionCounter = 2;
	TIM_TimeBaseInit(TIM1, &tp);

	TIM_ClearITPendingBit(TIM1, TIM_IT_Update);
	TIM_ITConfig(TIM1, TIM_IT_Update, ENABLE);

	NVIC_InitTypeDef np3;
	np3.NVIC_IRQChannel = TIM1_UP_TIM10_IRQn;
	np3.NVIC_IRQChannelPreemptionPriority = 0;
	np3.NVIC_IRQChannelSubPriority = 0;
	np3.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&np3);


	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);


	GPIO_PinAFConfig(GPIOA, GPIO_PinSource6, GPIO_AF_TIM3);

	gpio.GPIO_Pin = GPIO_Pin_6;
	gpio.GPIO_OType = GPIO_OType_PP;
	gpio.GPIO_PuPd = GPIO_PuPd_NOPULL;
	gpio.GPIO_Mode = GPIO_Mode_AF;
	gpio.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOA, &gpio);

	tp.TIM_Prescaler = 0;
	tp.TIM_CounterMode = TIM_CounterMode_Up;
	tp.TIM_Period = 0xffff;
	tp.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInit(TIM3, &tp);

	TIM_TIxExternalClockConfig(TIM3, TIM_TIxExternalCLK1Source_TI1, TIM_ICPolarity_BothEdge, 0);
	TIM_Cmd(TIM3, ENABLE);


	for (volatile size_t i = 0; i < 1000000; i++);

	TIM_Cmd(TIM1, ENABLE);

	while (1);


	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

	GPIO_PinAFConfig(GPIOA, GPIO_PinSource1, GPIO_AF_TIM2);

	gpio.GPIO_Pin = GPIO_Pin_1;
	gpio.GPIO_OType = GPIO_OType_PP;
	gpio.GPIO_PuPd = GPIO_PuPd_NOPULL;
	gpio.GPIO_Mode = GPIO_Mode_AF;
	gpio.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOA, &gpio);

	gpio.GPIO_Pin = GPIO_Pin_0;
	gpio.GPIO_OType = GPIO_OType_PP;
	gpio.GPIO_PuPd = GPIO_PuPd_NOPULL;
	gpio.GPIO_Mode = GPIO_Mode_OUT;
	gpio.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOA, &gpio);

	tp.TIM_Prescaler = 0;
	tp.TIM_CounterMode = TIM_CounterMode_Up;
	tp.TIM_Period = 840;
	tp.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInit(TIM2, &tp);

	// TIM_ClearITPendingBit(TIM2, TIM_IT_Trigger);
	// TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
	// TIM_ITConfig(TIM2, TIM_IT_Trigger, ENABLE);
	// TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);


	TIM_OCInitTypeDef oc2;

	oc2.TIM_OCMode = TIM_OCMode_PWM1;
	oc2.TIM_OutputState = TIM_OutputState_Enable;
	oc2.TIM_OCPolarity = TIM_OCPolarity_Low;
	oc2.TIM_Pulse = 1;

	TIM_OC2Init(TIM2, &oc2);



	//TIM_OC2PreloadConfig(TIM2, TIM_OCPreload_Disable);

	TIM_SelectOnePulseMode(TIM2, TIM_OPMode_Single);
	//TIM_SelectSlaveMode(TIM2, TIM_SlaveMode_Trigger);

	// TIM_GenerateEvent(TIM2, TIM_EventSource_Trigger);
	// TIM_GenerateEvent(TIM2, TIM_EventSource_Update);

	GPIO_ResetBits(GPIOA, GPIO_Pin_0);


	GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_TIM5);

	gpio.GPIO_Pin = GPIO_Pin_3;
	gpio.GPIO_OType = GPIO_OType_PP;
	gpio.GPIO_PuPd = GPIO_PuPd_NOPULL;
	gpio.GPIO_Mode = GPIO_Mode_AF;
	gpio.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOA, &gpio);


	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM5, ENABLE);

	tp.TIM_Prescaler = 0;
	tp.TIM_CounterMode = TIM_CounterMode_Up;
	tp.TIM_Period = 0xffffffff;
	tp.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInit(TIM5, &tp);

	TIM_ClearITPendingBit(TIM5, TIM_IT_CC4);
	TIM_ITConfig(TIM5, TIM_IT_CC4, ENABLE);
	TIM_Cmd(TIM5, ENABLE);


	TIM_ICInitTypeDef ic;
	ic.TIM_Channel = TIM_Channel_4;
	ic.TIM_ICPolarity = TIM_ICPolarity_BothEdge;
	ic.TIM_ICPrescaler = TIM_ICPSC_DIV1;
	ic.TIM_ICSelection = TIM_ICSelection_DirectTI;
	ic.TIM_ICFilter = 0;
	TIM_ICInit(TIM5, &ic);

	NVIC_InitTypeDef np2;
	np2.NVIC_IRQChannel = TIM5_IRQn;
	np2.NVIC_IRQChannelPreemptionPriority = 0;
	np2.NVIC_IRQChannelSubPriority = 0;
	np2.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&np2);


	for (volatile size_t i = 0; i < 1000000; i++);

	GPIOA->BSRRL = GPIO_Pin_0;
	TIM2->CR1 |= TIM_CR1_CEN;

	while (1) {
		// GPIO_ToggleBits(GPIOA, GPIO_Pin_1);
		for (volatile size_t i = 0; i < 40000000; i++);
		// TIM_Cmd(TIM2, ENABLE);
	}
	*/

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);

	tp.TIM_Prescaler = 0;
	tp.TIM_CounterMode = TIM_CounterMode_Up;
	tp.TIM_Period = 8400 - 1;
	tp.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInit(TIM4, &tp);

	TIM_Cmd(TIM4, ENABLE);

	TIM_OCInitTypeDef oc;

	oc.TIM_OCMode = TIM_OCMode_PWM1;
	oc.TIM_OutputState = TIM_OutputState_Enable;
	oc.TIM_OCPolarity = TIM_OCPolarity_High;
	oc.TIM_Pulse = 0;

	TIM_OC1Init(TIM4, &oc);
	TIM_OC1PreloadConfig(TIM4, TIM_OCPreload_Enable);

	oc.TIM_OCMode = TIM_OCMode_PWM2;
	TIM_OC2Init(TIM4, &oc);
	TIM_OC2PreloadConfig(TIM4, TIM_OCPreload_Enable);

	oc.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OC3Init(TIM4, &oc);
	TIM_OC3PreloadConfig(TIM4, TIM_OCPreload_Enable);

	oc.TIM_OCMode = TIM_OCMode_PWM2;
	TIM_OC4Init(TIM4, &oc);
	TIM_OC4PreloadConfig(TIM4, TIM_OCPreload_Enable);


	GPIO_PinAFConfig(GPIOD, GPIO_PinSource12, GPIO_AF_TIM4);
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource13, GPIO_AF_TIM4);
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource14, GPIO_AF_TIM4);
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource15, GPIO_AF_TIM4);

	gpio.GPIO_Pin = GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
	gpio.GPIO_OType = GPIO_OType_PP;
	gpio.GPIO_PuPd = GPIO_PuPd_NOPULL;
	gpio.GPIO_Mode = GPIO_Mode_AF;
	gpio.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOD, &gpio);


	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

	tp.TIM_Prescaler = 8400 - 1;
	tp.TIM_CounterMode = TIM_CounterMode_Up;
	tp.TIM_Period = 250 - 1;
	tp.TIM_ClockDivision = TIM_CKD_DIV1;

	TIM_TimeBaseInit(TIM2, &tp);
	TIM_Cmd(TIM2, ENABLE);

	TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
	TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);

	NVIC_InitTypeDef np;
	np.NVIC_IRQChannel = TIM2_IRQn;
	np.NVIC_IRQChannelPreemptionPriority = 0;
	np.NVIC_IRQChannelSubPriority = 0;
	np.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&np);

	while (1);


	gpio.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_12 | GPIO_Pin_13
			| GPIO_Pin_14 | GPIO_Pin_15;
	gpio.GPIO_Mode = GPIO_Mode_OUT;
	gpio.GPIO_OType = GPIO_OType_PP;
	gpio.GPIO_PuPd = GPIO_PuPd_NOPULL;
	gpio.GPIO_Speed = GPIO_Speed_2MHz;

	GPIO_Init(GPIOD, &gpio);

	gpio.GPIO_Pin = GPIO_Pin_2;
	gpio.GPIO_Speed = GPIO_Speed_25MHz;
	GPIO_Init(GPIOD, &gpio);

	gpio.GPIO_Pin = GPIO_Pin_4;
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOD, &gpio);

	gpio.GPIO_Pin = GPIO_Pin_6;
	gpio.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(GPIOD, &gpio);

	GPIO_ResetBits(GPIOD, GPIO_Pin_0 | GPIO_Pin_2 | GPIO_Pin_4 | GPIO_Pin_6);

	gpio.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_5;
	gpio.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOA, &gpio);

	gpio.GPIO_Pin = GPIO_Pin_5;
	GPIO_Init(GPIOC, &gpio);

	int *p = NULL;
	int *q = NULL;
	p = malloc(1024);
	q = malloc(1024);
	free(p);
	p = malloc(2048);
	free(q);
	q = malloc(3072);


	// Tasks to blink LEDs and toggle LA pins
	BaseType_t rv = xTaskCreate(led, "led0", 400, (void*)&led0, 0, NULL);
	if (rv != pdPASS) {
		while (1);
	}

	rv = xTaskCreate(led, "led1", 400, (void*)&led1, 0, NULL);
	if (rv != pdPASS) {
		while (1);
	}

	rv = xTaskCreate(led, "led2", 400, (void*)&led2, 0, NULL);
	if (rv != pdPASS) {
		while (1);
	}

	rv = xTaskCreate(led, "led3", 400, (void*)&led3, 0, NULL);
	if (rv != pdPASS) {
		while (1);
	}

	rv = xTaskCreate(led, "la0", 400, (void*)&la0, 0, NULL);
	if (rv != pdPASS) {
		while (1);
	}

	rv = xTaskCreate(led, "la1", 400, (void*)&la1, 0, NULL);
	if (rv != pdPASS) {
		while (1);
	}

	rv = xTaskCreate(led, "la2", 400, (void*)&la2, 0, NULL);
	if (rv != pdPASS) {
		while (1);
	}

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM8, ENABLE);

	TIM_TimeBaseInitTypeDef time;

	time.TIM_CounterMode = TIM_CounterMode_Up;
	time.TIM_ClockDivision = TIM_CKD_DIV1;
	time.TIM_Prescaler = 16800 - 1;
	time.TIM_Period = 5000 - 1;
	time.TIM_RepetitionCounter = 0;

	TIM_TimeBaseInit(TIM8, &time);
	TIM_ITConfig(TIM8, TIM_IT_Update, ENABLE);
	//TIM_Cmd(TIM8, ENABLE);


	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, ENABLE);
	DMA_InitTypeDef dma;
	DMA_DeInit(DMA2_Stream1);

	uint16_t data[] = {0xffff, 0x0000};

	dma.DMA_Channel = DMA_Channel_7;
	dma.DMA_PeripheralBaseAddr = (uint32_t)&GPIOD->ODR;
	dma.DMA_Memory0BaseAddr = (uint32_t)data;
	dma.DMA_DIR = DMA_DIR_MemoryToPeripheral;
	dma.DMA_BufferSize = sizeof(data) / sizeof(data[0]);
	dma.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	dma.DMA_MemoryInc = DMA_MemoryInc_Enable;
	dma.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	dma.DMA_MemoryDataSize = DMA_PeripheralDataSize_HalfWord;
	dma.DMA_Mode = DMA_Mode_Circular;
	dma.DMA_Priority = DMA_Priority_High;
	dma.DMA_FIFOMode = DMA_FIFOMode_Disable;
	dma.DMA_FIFOThreshold = DMA_FIFOThreshold_Full;
	dma.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	dma.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;

	//DMA_Init(DMA2_Stream1, &dma);

	//DMA_Cmd(DMA2_Stream1, ENABLE);
	//TIM_DMACmd(TIM8, TIM_DMA_Update, ENABLE);

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

	TIM_TimeBaseInitTypeDef t;
	t.TIM_Prescaler = 0;
	t.TIM_CounterMode = TIM_CounterMode_Up;
	t.TIM_Period = 84 - 1;
	t.TIM_ClockDivision = TIM_CKD_DIV1;

	TIM_TimeBaseInit(TIM2, &t);
	//TIM_Cmd(TIM2, ENABLE);

	TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
	//TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);


	NVIC_InitTypeDef n;
	n.NVIC_IRQChannel = TIM2_IRQn;
	n.NVIC_IRQChannelPreemptionPriority = 1;
	n.NVIC_IRQChannelSubPriority = 0;
	n.NVIC_IRQChannelCmd = ENABLE;
	//NVIC_Init(&n);

	t.TIM_Prescaler = 0;
	t.TIM_CounterMode = TIM_CounterMode_Up;
	t.TIM_Period = 84 - 1;
	t.TIM_ClockDivision = TIM_CKD_DIV1;

	TIM_TimeBaseInit(TIM3, &t);
	//TIM_Cmd(TIM3, ENABLE);

	TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
	//TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);

	n.NVIC_IRQChannel = TIM3_IRQn;
	n.NVIC_IRQChannelPreemptionPriority = 0;
	n.NVIC_IRQChannelSubPriority = 0;
	n.NVIC_IRQChannelCmd = ENABLE;
	//NVIC_Init(&n);

	vTaskStartScheduler();

	// Won't be reached
	while (1) {
		g1++;
		g2++;
		//p++;
		//q++;

		GPIO_ToggleBits(GPIOD, GPIO_Pin_13);
		GPIO_ToggleBits(GPIOD, GPIO_Pin_12);
		GPIO_ToggleBits(GPIOD, GPIO_Pin_14);
		GPIO_ToggleBits(GPIOD, GPIO_Pin_15);

		for (volatile size_t i = 0; i < 10000000; i++);
	}
}


void TIM2_IRQHandler() {

	static int16_t inc = -200;

	if (TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET) {
		TIM_ClearITPendingBit(TIM2, TIM_IT_Update);

		if (TIM4->CCR4 < 200) inc = -inc;
		else if (TIM4->CCR4 >= 8200) inc = -inc;

		TIM4->CCR1 = (TIM4->CCR1 + inc) % 8400;
		TIM4->CCR2 = (TIM4->CCR2 + inc) % 8400;
		TIM4->CCR3 = (TIM4->CCR3 + inc) % 8400;
		TIM4->CCR4 = (TIM4->CCR4 + inc) % 8400;
	}
}


void TIM3_IRQHandler() {

	if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET) {
		//TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
		TIM3->SR = (uint16_t)~TIM_IT_Update;

		GPIOD->ODR ^= GPIO_Pin_0 | GPIO_Pin_2 | GPIO_Pin_4 | GPIO_Pin_6;
	}
}


void TIM5_IRQHandler(void) {

	static uint32_t val;

	if (TIM_GetITStatus(TIM5, TIM_IT_CC4) != RESET) {
		TIM_ClearITPendingBit(TIM5, TIM_IT_CC4);

		if (GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_3)) {
			val = TIM5->CCR4;

		} else {
			volatile uint32_t t = TIM5->CCR4;
		}
	}
}


void TIM1_UP_TIM10_IRQHandler(void) {

	static size_t count;

	if (TIM_GetITStatus(TIM1, TIM_IT_Update) != RESET) {
		TIM_ClearITPendingBit(TIM1, TIM_IT_Update);

		if (++count == 1000) TIM_Cmd(TIM1, DISABLE);
	}
}


void vApplicationMallocFailedHook(void) {
	while (1);
}

void vApplicationStackOverflowHook(TaskHandle_t xTask, char *pcTaskName) {
	while (1);
}
