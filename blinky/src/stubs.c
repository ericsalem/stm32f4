#include <sys/stat.h>


/*
* Get rid of obnoxious false warnings about not implementing syscalls when using nosys.specs
* This happens due to enabling hardware floating point arithmetic
* It's triggered in FreeRTOS's GCC port of ARM_CM4F
*/

__attribute__((weak)) int _close(int file) {
	return -1;
}

__attribute__((weak)) int _read(int file, char *ptr, int len) {
	return -1;
}

__attribute__((weak)) int _lseek(int file, int ptr, int dir) {
	return -1;
}

__attribute__((weak)) int _write(int file, char *ptr, int len) {
	return -1;
}

/*
* Need these functions for sprintf()
*/

__attribute__((weak)) int _fstat(int file, struct stat *st) {
	return -1;
}

__attribute__((weak)) int _isatty(int file) {
	return -1;
}

__attribute__((weak)) int _getpid(void) {
	return -1;
}

__attribute__((weak)) void _kill(int pid, int sig) {
	return;
}
