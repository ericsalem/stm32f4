#include <stddef.h>
#include <string.h>
#include <stm32f4xx.h>
#include <FreeRTOS.h>
#include <semphr.h>
#include "uart.h"


#define NUM_UARTS	6

/*
uart1: dma2 stream7 channel4
uart2: dma1 stream6 channel4
uart3: dma1 stream3 channel4
uart4: dma1 stream4 channel4
uart5: dma1 stream7 channel4
uart6: dma2 stream6 channel5
*/

typedef struct {
	USART_TypeDef *uart;
	SemaphoreHandle_t mtx_uart;
	SemaphoreHandle_t sem_dma;
	DMA_Stream_TypeDef *stream_dma;
	uint32_t stream_int;
	DMA_InitTypeDef dma;
} uart_ipc_t;


static uart_ipc_t uarts[NUM_UARTS];


static void uart_init_gpio(uint8_t af, GPIO_TypeDef *group_tx, GPIO_TypeDef *group_rx, uint16_t tx, uint16_t rx);
static uint32_t uart_get_periph(GPIO_TypeDef *group);
static uint8_t uart_get_ps(uint16_t pin);

static void uart_init_uart(USART_TypeDef *uart, uint32_t baud, void (*clk_fn)(uint32_t, FunctionalState), uint32_t periph);
static void uart_init_dma(uart_t u, USART_TypeDef *uart, uint32_t dma_periph, DMA_Stream_TypeDef *stream, IRQn_Type dma_irq, uint8_t dma_pri, uint32_t stream_int, uint32_t channel);


int uart_init(uart_t u, uint32_t baud, GPIO_TypeDef *group_tx, GPIO_TypeDef *group_rx, uint16_t tx, uint16_t rx, uint8_t dma_pri) {

	uint8_t af;
	void (*fn)(uint32_t, FunctionalState);
	uint32_t periph;
	USART_TypeDef *uart;

	switch (u) {

		case UART_1:
			af = GPIO_AF_USART1;
			uart = USART1;
			fn = RCC_APB2PeriphClockCmd;
			periph = RCC_APB2Periph_USART1;

			uart_init_dma(u, uart, RCC_AHB1Periph_DMA2, DMA2_Stream7, DMA2_Stream7_IRQn, dma_pri, DMA_IT_TCIF7, DMA_Channel_4);
			break;

		case UART_2:
			af = GPIO_AF_USART2;
			uart = USART2;
			fn = RCC_APB1PeriphClockCmd;
			periph = RCC_APB1Periph_USART2;

			uart_init_dma(u, uart, RCC_AHB1Periph_DMA1, DMA1_Stream6, DMA1_Stream6_IRQn, dma_pri, DMA_IT_TCIF6, DMA_Channel_4);
			break;

		case UART_3:
			af = GPIO_AF_USART3;
			uart = USART3;
			fn = RCC_APB1PeriphClockCmd;
			periph = RCC_APB1Periph_USART3;

			uart_init_dma(u, uart, RCC_AHB1Periph_DMA1, DMA1_Stream3, DMA1_Stream3_IRQn, dma_pri, DMA_IT_TCIF3, DMA_Channel_4);
			break;

		case UART_4:
			af = GPIO_AF_UART4;
			uart = UART4;
			fn = RCC_APB1PeriphClockCmd;
			periph = RCC_APB1Periph_UART4;

			uart_init_dma(u, uart, RCC_AHB1Periph_DMA1, DMA1_Stream4, DMA1_Stream4_IRQn, dma_pri, DMA_IT_TCIF4, DMA_Channel_4);
			break;

		case UART_5:
			af = GPIO_AF_UART5;
			uart = UART5;
			fn = RCC_APB1PeriphClockCmd;
			periph = RCC_APB1Periph_UART5;

			uart_init_dma(u, uart, RCC_AHB1Periph_DMA1, DMA1_Stream7, DMA1_Stream7_IRQn, dma_pri, DMA_IT_TCIF7, DMA_Channel_4);
			break;

		case UART_6:
			af = GPIO_AF_USART6;
			uart = USART6;
			fn = RCC_APB2PeriphClockCmd;
			periph = RCC_APB2Periph_USART6;

			uart_init_dma(u, uart, RCC_AHB1Periph_DMA2, DMA2_Stream6, DMA2_Stream6_IRQn, dma_pri, DMA_IT_TCIF6, DMA_Channel_5);
			break;

		default:
			return -1;
	}

	uart_init_uart(uart, baud, fn, periph);
	uart_init_gpio(af, group_tx, group_rx, tx, rx);

	return 0;
}


int uart_int_en(uart_t u, uint8_t uart_pri) {

	USART_TypeDef *uart;
	IRQn_Type uart_irq;

	switch (u) {

		case UART_1:
			uart = USART1;
			uart_irq = USART1_IRQn;
			break;

		case UART_2:
			uart = USART2;
			uart_irq = USART2_IRQn;
			break;

		case UART_3:
			uart = USART3;
			uart_irq = USART3_IRQn;
			break;

		case UART_4:
			uart = UART4;
			uart_irq = UART4_IRQn;
			break;

		case UART_5:
			uart = UART5;
			uart_irq = UART5_IRQn;
			break;

		case UART_6:
			uart = USART6;
			uart_irq = USART6_IRQn;
			break;

		default:
			return -1;
	}

	USART_ClearITPendingBit(uart, USART_IT_RXNE);
	USART_ITConfig(uart, USART_IT_RXNE, ENABLE);

	NVIC_InitTypeDef nvic;
	nvic.NVIC_IRQChannel = uart_irq;
	nvic.NVIC_IRQChannelPreemptionPriority = uart_pri;
	nvic.NVIC_IRQChannelSubPriority = 0;
	nvic.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&nvic);

	return 0;
}


int uart_tx(uart_t u, uint8_t *buf, size_t sz, bool dma) {

	if (u > UART_6) return -1;
	else if (!uarts[u].mtx_uart) return -2;

	// Synchronize access to UART
	xSemaphoreTake(uarts[u].mtx_uart, portMAX_DELAY);

	if (!dma) {
		for (size_t i = 0; i < sz; i++) {
			while (USART_GetFlagStatus(uarts[u].uart, USART_FLAG_TXE) == RESET);

			USART_SendData(uarts[u].uart, buf[i]);
		}

	} else {
		DMA_DeInit(uarts[u].stream_dma);

		uarts[u].dma.DMA_BufferSize = sz;
		uarts[u].dma.DMA_Memory0BaseAddr = (uint32_t)buf;

		DMA_Init(uarts[u].stream_dma, &uarts[u].dma);
		DMA_ClearITPendingBit(uarts[u].stream_dma, uarts[u].stream_int);
		DMA_ITConfig(uarts[u].stream_dma, DMA_IT_TC, ENABLE);

		// Kick off DMA
		DMA_Cmd(uarts[u].stream_dma, ENABLE);

		// Wait for DMA transfer to complete
		xSemaphoreTake(uarts[u].sem_dma, portMAX_DELAY);
	}

	// Done with the UART so release the lock
	xSemaphoreGive(uarts[u].mtx_uart);

	return 0;
}


static void uart_init_gpio(uint8_t af, GPIO_TypeDef *group_tx, GPIO_TypeDef *group_rx, uint16_t tx, uint16_t rx) {

	GPIO_InitTypeDef gpio;
	uint32_t periph_tx = uart_get_periph(group_tx), periph_rx = uart_get_periph(group_rx);

	RCC_AHB1PeriphClockCmd(periph_tx, ENABLE);
	RCC_AHB1PeriphClockCmd(periph_rx, ENABLE);

	GPIO_PinAFConfig(group_tx, uart_get_ps(tx), af);
	GPIO_PinAFConfig(group_rx, uart_get_ps(rx), af);

	gpio.GPIO_Pin = tx;
	gpio.GPIO_OType = GPIO_OType_PP;
	gpio.GPIO_PuPd = GPIO_PuPd_NOPULL;
	gpio.GPIO_Mode = GPIO_Mode_AF;
	gpio.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(group_tx, &gpio);

	gpio.GPIO_Pin = rx;
	GPIO_Init(group_rx, &gpio);
}


static uint32_t uart_get_periph(GPIO_TypeDef *group) {

	if (group == GPIOA) return RCC_AHB1Periph_GPIOA;
	else if (group == GPIOB) return RCC_AHB1Periph_GPIOB;
	else if (group == GPIOC) return RCC_AHB1Periph_GPIOC;
	else if (group == GPIOD) return RCC_AHB1Periph_GPIOD;
	else if (group == GPIOE) return RCC_AHB1Periph_GPIOE;
	else if (group == GPIOF) return RCC_AHB1Periph_GPIOF;
	else if (group == GPIOG) return RCC_AHB1Periph_GPIOG;
	else if (group == GPIOH) return RCC_AHB1Periph_GPIOH;
	else if (group == GPIOI) return RCC_AHB1Periph_GPIOI;
	else if (group == GPIOJ) return RCC_AHB1Periph_GPIOJ;
	else return RCC_AHB1Periph_GPIOK;
}


// Get pin source
static uint8_t uart_get_ps(uint16_t pin) {

	switch (pin) {
		case GPIO_Pin_0:
			return GPIO_PinSource0;

		case GPIO_Pin_1:
			return GPIO_PinSource1;

		case GPIO_Pin_2:
			return GPIO_PinSource2;

		case GPIO_Pin_3:
			return GPIO_PinSource3;

		case GPIO_Pin_4:
			return GPIO_PinSource4;

		case GPIO_Pin_5:
			return GPIO_PinSource5;

		case GPIO_Pin_6:
			return GPIO_PinSource6;

		case GPIO_Pin_7:
			return GPIO_PinSource7;

		case GPIO_Pin_8:
			return GPIO_PinSource8;

		case GPIO_Pin_9:
			return GPIO_PinSource9;

		case GPIO_Pin_10:
			return GPIO_PinSource10;

		case GPIO_Pin_11:
			return GPIO_PinSource11;

		case GPIO_Pin_12:
			return GPIO_PinSource12;

		case GPIO_Pin_13:
			return GPIO_PinSource13;

		case GPIO_Pin_14:
			return GPIO_PinSource14;

		default:
			return GPIO_PinSource15;
	}
}


static void uart_init_uart(USART_TypeDef *uart, uint32_t baud, void (*clk_fn)(uint32_t, FunctionalState), uint32_t periph) {

	USART_InitTypeDef cfg;
	cfg.USART_BaudRate = baud;
	cfg.USART_WordLength = USART_WordLength_8b;
	cfg.USART_StopBits = USART_StopBits_1;
	cfg.USART_Parity = USART_Parity_No;
	cfg.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	cfg.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

	clk_fn(periph, ENABLE);
	USART_Init(uart, &cfg);
	USART_Cmd(uart, ENABLE);

	USART_DMACmd(uart, USART_DMAReq_Tx, ENABLE);
}


static void uart_init_dma(uart_t u, USART_TypeDef *uart, uint32_t dma_periph, DMA_Stream_TypeDef *stream, IRQn_Type dma_irq, uint8_t dma_pri, uint32_t stream_int, uint32_t channel) {

	uarts[u].dma.DMA_Channel = channel;
	uarts[u].dma.DMA_PeripheralBaseAddr = (uint32_t)&uart->DR;
	uarts[u].dma.DMA_Memory0BaseAddr = (uint32_t)NULL;
	uarts[u].dma.DMA_DIR = DMA_DIR_MemoryToPeripheral;
	uarts[u].dma.DMA_BufferSize = 0;
	uarts[u].dma.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	uarts[u].dma.DMA_MemoryInc = DMA_MemoryInc_Enable;
	uarts[u].dma.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	uarts[u].dma.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	uarts[u].dma.DMA_Mode = DMA_Mode_Normal;
	uarts[u].dma.DMA_Priority = DMA_Priority_Low;
	uarts[u].dma.DMA_FIFOMode = DMA_FIFOMode_Disable;
	uarts[u].dma.DMA_FIFOThreshold = DMA_FIFOThreshold_1QuarterFull;
	uarts[u].dma.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	uarts[u].dma.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;

	uarts[u].uart = uart;
	if (!uarts[u].mtx_uart) uarts[u].mtx_uart = xSemaphoreCreateMutex();
	if (!uarts[u].sem_dma) uarts[u].sem_dma = xSemaphoreCreateBinary();
	uarts[u].stream_dma = stream;
	uarts[u].stream_int = stream_int;

	RCC_AHB1PeriphClockCmd(dma_periph, ENABLE);

	NVIC_InitTypeDef nvic;
	nvic.NVIC_IRQChannel = dma_irq;
	nvic.NVIC_IRQChannelPreemptionPriority = dma_pri;
	nvic.NVIC_IRQChannelSubPriority = 0;
	nvic.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&nvic);
}


// UART1 DMA handler
void DMA2_Stream7_IRQHandler(void) {

	if (DMA_GetITStatus(DMA2_Stream7, DMA_IT_TCIF7) != RESET) {
        DMA_ClearITPendingBit(DMA2_Stream7, DMA_IT_TCIF7);

		BaseType_t task = pdFALSE;

		// Signal task DMA transfer is complete
		xSemaphoreGiveFromISR(uarts[UART_1].sem_dma, &task);

		portYIELD_FROM_ISR(task);
    }
}


// UART2 DMA handler
void DMA1_Stream6_IRQHandler(void) {

	if (DMA_GetITStatus(DMA1_Stream6, DMA_IT_TCIF6) != RESET) {
        DMA_ClearITPendingBit(DMA1_Stream6, DMA_IT_TCIF6);

		BaseType_t task = pdFALSE;

		// Signal task DMA transfer is complete
		xSemaphoreGiveFromISR(uarts[UART_2].sem_dma, &task);

		portYIELD_FROM_ISR(task);
    }
}


// UART3 DMA handler
void DMA1_Stream3_IRQHandler(void) {

	if (DMA_GetITStatus(DMA1_Stream3, DMA_IT_TCIF3) != RESET) {
        DMA_ClearITPendingBit(DMA1_Stream3, DMA_IT_TCIF3);

		BaseType_t task = pdFALSE;

		// Signal task DMA transfer is complete
		xSemaphoreGiveFromISR(uarts[UART_3].sem_dma, &task);

		portYIELD_FROM_ISR(task);
    }
}


// UART4 DMA handler
void DMA1_Stream4_IRQHandler(void) {

	if (DMA_GetITStatus(DMA1_Stream4, DMA_IT_TCIF4) != RESET) {
        DMA_ClearITPendingBit(DMA1_Stream4, DMA_IT_TCIF4);

		BaseType_t task = pdFALSE;

		// Signal task DMA transfer is complete
		xSemaphoreGiveFromISR(uarts[UART_4].sem_dma, &task);

		portYIELD_FROM_ISR(task);
    }
}


// UART5 DMA handler
void DMA1_Stream7_IRQHandler(void) {

	if (DMA_GetITStatus(DMA1_Stream7, DMA_IT_TCIF7) != RESET) {
        DMA_ClearITPendingBit(DMA1_Stream7, DMA_IT_TCIF7);

		BaseType_t task = pdFALSE;

		// Signal task DMA transfer is complete
		xSemaphoreGiveFromISR(uarts[UART_5].sem_dma, &task);

		portYIELD_FROM_ISR(task);
    }
}


// UART6 DMA handler
void DMA2_Stream6_IRQHandler(void) {

	if (DMA_GetITStatus(DMA2_Stream6, DMA_IT_TCIF6) != RESET) {
        DMA_ClearITPendingBit(DMA2_Stream6, DMA_IT_TCIF6);

		BaseType_t task = pdFALSE;

		// Signal task DMA transfer is complete
		xSemaphoreGiveFromISR(uarts[UART_6].sem_dma, &task);

		portYIELD_FROM_ISR(task);
    }
}
