#pragma once


#include <stdint.h>
#include <stdbool.h>
#include <stm32f4xx.h>


typedef enum {
    UART_1,
    UART_2,
	UART_3,
	UART_4,
	UART_5,
	UART_6
} uart_t;


int uart_init(uart_t u, uint32_t baud, GPIO_TypeDef *group_tx, GPIO_TypeDef *group_rx, uint16_t tx, uint16_t rx, uint8_t dma_pri);
int uart_int_en(uart_t u, uint8_t uart_pri);
int uart_tx(uart_t u, uint8_t *buf, size_t sz, bool dma);
