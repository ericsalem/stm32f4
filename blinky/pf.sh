#!/bin/bash

gdb \
	--batch \
	-ex "target extended-remote :3333" \
	-ex "monitor reset halt" \
	-ex "load" \
	-ex "monitor reset" \
	build/blinky.elf
