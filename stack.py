#!/usr/bin/env python3

import glob
import re
import sys
import io


# https://gcc.gnu.org/onlinedocs/gcc/Developer-Options.html
# Use -fstack-usage -fcallgraph-info=su
#
# GCC can't generate info for function pointer calls (__indirect_call)


# https://stackoverflow.com/questions/287871/how-do-i-print-colored-text-to-the-terminal
RED = "\033[31m"
GREEN = "\033[32m"
BLUE = "\033[94m"
YELLOW = "\33[33m"
BEIGE = "\33[36m"
BEIGE2 = "\33[96m"
VIOLET = "\33[35m"
BOLD = "\33[1m"
RESET = "\033[0m"

# Use colors only when printing to a terminal
if not sys.stdout.isatty():
	RED = GREEN = BLUE = YELLOW = BEIGE = BEIGE2 = VIOLET = BOLD = RESET = ""

# Call graph information
ci = {}
reNode = re.compile(r'^node:\s+{\s+title:\s+"([^"]+)"\s+label:\s+"([^:]+):([^:]+):(\d+):(\d+):(\d+)\s+bytes?\s+\((static|dynamic|bounded)\)"\s+}$', re.IGNORECASE)
reEdge = re.compile(r'^edge:\s+{\s+sourcename:\s+"([^"]+)"\s+targetname:\s+"([^"]+)"\s+label:\s+"[^"]+"\s+}$', re.IGNORECASE)

# Used to clean up paths
reAbs = re.compile(r"^.+newlib/", re.IGNORECASE)
reDot = re.compile(r"^(?:\.\.?/)+", re.IGNORECASE)


def print_su(ci, key, level, sz, seq, f):

	indent = "    "

	if key not in ci:
		print(f"{indent * level}{BEIGE}{key}{RESET} (info not available)", file = f)
		return sz

	fn = ci[key]
	total = sz + fn["sz"]

	rec = f"{BOLD}{RED}RECURSIVE{RESET} " if key in seq else ""

	name = f'{rec}{BEIGE}{fn["name"]}{RESET} ({f"{BLUE}static" if ":" in key else f"{YELLOW}global"}{RESET} at {VIOLET}{fn["src"]}{RESET}:{BEIGE2}{fn["line"]}{RESET})'

	print(f'{indent * level}{name} ({GREEN}{fn["sz"]}{RESET} bytes, {RED}{total}{RESET} total{"" if level else ", max: placeholder"})', file = f)

	max_sz = total

	for callee in sorted(fn["calls"], key = lambda x: x.split(":")[-1].lower() if ":" in x else x.lower()):
		if not (callee in seq and rec):
			sz = print_su(ci, callee, level + 1, total, seq + [key], f)
			if sz > max_sz:
				max_sz = sz

	return max_sz


# Parse call graph
for path in glob.glob("**/*.ci", recursive = True):
	proj = path.split("/")[0]

	with open(path, "r") as f:
		lines = f.readlines()

		# We need to parse nodes before edges, so loop over the file twice
		for line in lines:

			m = reNode.search(line.replace(r"\n", ":"))

			if m:
				title = m.group(1)
				fn = m.group(2)
				src = m.group(3)
				line = m.group(4)
				col = m.group(5)
				sz = m.group(6)
				qual = m.group(7)

				# Prefix static functions with their path
				key = f"{proj}/{title}" if ":" in title else title

				# There should never be a dupe
				if key in ci:
					raise Exception(f"Duplicate key found: {key}")

				elif src.startswith("/"):
					src = reAbs.sub("newlib/", src)

				elif src.startswith("."):
					src = reDot.sub("", src)

				ci[key] = {
					"name": fn,
					"src": f"{proj}/{src}",
					"line": int(line),
					"col": int(col),
					"sz": int(sz),
					"qual": qual,
					"calls": set()
				}

		# Now parse edges
		for line in lines:

			m = reEdge.search(line)

			if m:
				# If the source name is prefixed with the graph's title (file path), it's a static function
				sn = m.group(1)
				key = f"{proj}/{sn}" if ":" in sn else sn

				# Same for target name
				tn = m.group(2)
				callee = f"{proj}/{tn}" if ":" in tn else tn

				ci[key]["calls"].add(callee)


# for key in sorted(ci):
# 	print_su(ci, key, 0, 0, [])

for fn in ["main"]:
	f = io.StringIO()

	sz = print_su(ci, fn, 0, 0, [], f)

	print(f.getvalue().replace("max: placeholder)", f"max: {BOLD}{RED}{sz}{RESET})"))
