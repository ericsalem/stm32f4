#include <FreeRTOS.h>
#include <task.h>

__attribute__((weak)) void vApplicationMallocFailedHook(void) {
	while (1);
}

__attribute__((weak)) void vApplicationStackOverflowHook(TaskHandle_t xTask, char *pcTaskName) {
	while (1);
}
