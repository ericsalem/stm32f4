SHELL	:= /bin/bash
MAKEFLAGS += -j8 -O

project	:= $(notdir $(CURDIR))
blddir	:= build
headers	:= $(sort $(shell find -type f -name "*.h" -printf "%P\n"))

armprfx	:= arm-none-eabi
AR		:= $(armprfx)-ar
CC		:= $(armprfx)-gcc
OBJCOPY	:= $(armprfx)-objcopy

MKDIR	:= mkdir -p
RM		:= rm -rf

# A bug exists in ccache 4.9.1 but is fixed in later versions. Once Fedora has an updated package, remove this environment variable
export CCACHE_DISABLE := 1

CPPFLAGS := $(addprefix -I,$(CPPFLAGS))

CFLAGS	:= -ggdb3
CFLAGS	+= -Wall -Wextra -Wpedantic -Wshadow -Wdouble-promotion -Wundef -Wno-unused-parameter
CFLAGS	+= -fno-common -fstack-usage -fcallgraph-info=su -fdiagnostics-color=always -frecord-gcc-switches
TARGET_ARCH := -mlittle-endian -mthumb -march=armv7e-m+fp -mtune=cortex-m4 -mcpu=cortex-m4 -mno-thumb-interwork -mfloat-abi=hard -mfpu=fpv4-sp-d16

# If you want to minimize code size
#CFLAGS	+= -fdata-sections -ffunction-sections
