https://stackoverflow.com/questions/50154137/how-to-rebuild-newlib-and-newlib-nano-of-gnu-arm-embedded-toolchain

https://gist.github.com/iori-yja/9271860

https://newlib.sourceware.narkive.com/vquAvcam/problem-trying-to-configure-build-when-using-program-prefix-option

https://newlib.sourceware.narkive.com/sMuLS0cD/am-i-crazy-does-build-nothing

https://medium.com/my-gsoc-2019-journey/build-newlib-for-sparc-and-arm-architecture-6b3287d4c6f2

https://answers.launchpad.net/gcc-arm-embedded/+question/668554

https://answers.launchpad.net/gcc-arm-embedded/+question/267363

https://balau82.wordpress.com/2010/12/16/using-newlib-in-arm-bare-metal-programs/

https://stackoverflow.com/questions/43320597/how-do-i-build-newlib-for-size-optimization

https://ecos.sourceware.org/ml/newlib/current/018127.html

git clone https://sourceware.org/git/newlib-cygwin.git

export CFLAGS_FOR_TARGET=-fcallgraph-info=su

./configure --target=arm-none-eabi --disable-newlib-supplied-syscalls CFLAGS_FOR_TARGET=-fcallgraph-info=su

make -j8 CFLAGS_FOR_TARGET=-fcallgraph-info=su


ln -s ../newlib/arm-none-eabi/thumb/v7e-m+fp/hard/newlib/libc/sys/arm arm
ln -s ../newlib/arm-none-eabi/thumb/v7e-m+fp/hard/newlib/libm/common common
ln -s ../newlib/arm-none-eabi/thumb/v7e-m+fp/hard/newlib/libc/ctype ctype
ln -s ../newlib/arm-none-eabi/thumb/v7e-m+fp/hard/newlib/libc/errno errno
ln -s ../newlib/arm-none-eabi/thumb/v7e-m+fp/hard/libgloss/libnosys libnosys
ln -s ../newlib/arm-none-eabi/thumb/v7e-m+fp/hard/newlib/libc/locale locale
ln -s ../newlib/arm-none-eabi/thumb/v7e-m+fp/hard/newlib/libm/math math
ln -s ../newlib/arm-none-eabi/thumb/v7e-m+fp/hard/newlib/libc/reent reent
ln -s ../newlib/arm-none-eabi/thumb/v7e-m+fp/hard/newlib/libc/signal signal
ln -s ../newlib/arm-none-eabi/thumb/v7e-m+fp/hard/newlib/libc/stdio stdio
ln -s ../newlib/arm-none-eabi/thumb/v7e-m+fp/hard/newlib/libc/stdlib stdlib
ln -s ../newlib/arm-none-eabi/thumb/v7e-m+fp/hard/newlib/libc/string string
ln -s ../newlib/arm-none-eabi/thumb/v7e-m+fp/hard/newlib/libc/syscalls syscalls
